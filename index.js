const findVowelsCount = (
  // Task 1
  string
) =>
  string
    .split("")
    .filter((letter) => ["a", "e", "u", "i", "o"].indexOf(letter) !== -1)
    .length;

const findLeftDaysCount = () =>
  // Task 2
  (
    Math.round(
      new Date(new Date().getFullYear() + 1, 0, 1).getTime() -
        new Date().getTime()
    ) /
    (1000 * 60 * 60 * 24)
  ).toFixed();

const findSpecSymbols = (
  // Task 3
  array
) =>
  array.filter((symbol) =>
    /[ `!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?~]/.test(symbol)
  );

const findIntersection = (
  arr1,
  arr2 // Task 4
) => arr1.filter((element) => arr2.indexOf(element) !== -1);

const alertAfterDelay = (message, delay) => {
  // Task 5
  setTimeout(() => alert(message), delay * 1000);
};

const findNaturalNumbers = (
  array // Task 6
) => array.filter((number) => number > 0 && Number.isInteger(number));

const findChar = (
  string,
  target // Task 7
) => string.split("").filter((letter) => letter === target).length;

console.log("1:", findVowelsCount("abbos"));
console.log("2:", findLeftDaysCount());
console.log("3:", findSpecSymbols(["a", "@", "%", "s", "1"]));
console.log("4:", findIntersection([1, 2, 3, 4], [3, 4, 5, 6]));
console.log("5:", alertAfterDelay("Hello", 4));
console.log("6:", findNaturalNumbers([-1, 0, 1, 2, 3, 4]));
console.log("7:", findChar("abbos", "b"));
